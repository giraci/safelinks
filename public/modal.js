// Get the modal
var modal = document.getElementById("myModal");
var ubHeader = document.getElementById("ub-header");
var phishHeader = document.getElementById("phish-header");

// Get the button that opens the modal
var btn = document.getElementById("login-button");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

var user = document.getElementById("login");
var pass = document.getElementById("password");

var timer = setTimeout(showModal, 10000);

function cancelTimer() {
    clearTimeout(timer)
}

function showModal() {
    document.getElementById("password").value = ''
    modal.style.display = "block";
    ubHeader.style.display = "none";
    phishHeader.style.display = "block";
    cancelTimer()
}

function hideModal() {
    phishHeader.style.display = "none";
    ubHeader.style.display = "block";
    modal.style.display = "none";
    document.getElementById("password").value = ""

}

// When the user clicks on the button, open the modal
btn.onclick = showModal
pass.onkeydown = showModal

// When the user clicks on <span> (x), close the modal
span.onclick = hideModal

user.onkeydown = cancelTimer

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        hideModal();
    }
}

pass.disabled = false;